const nav = [
    {text: '首页', link: '/'},
    {text: '组件', link: '/components/e-icon-picker/'},
    {text: 'v1.x', link: 'https://e-icon-picker.cnovel.club'},
]

export default nav
