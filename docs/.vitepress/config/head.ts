const head = [
    ['link', {rel: 'icon', href: '/img/favicon.ico'}],
    ['link', {rel: 'stylesheet', href: '//unpkg.com/font-awesome/css/font-awesome.min.css'}],
    ['link', {rel: 'stylesheet', href: '//unpkg.com/element-ui/lib/theme-chalk/icon.css'}],
    ['link', {rel: 'stylesheet', href: '//cdn.bootcdn.net/ajax/libs/font-awesome/6.2.1/css/all.min.css'}],
    ['link', {rel: 'stylesheet', href: '//cdn.bootcdn.net/ajax/libs/font-awesome/6.2.1/css/brands.min.css'}],
    ['link', {rel: 'stylesheet', href: '//cdn.bootcdn.net/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css'}],
    ['link', {rel: 'stylesheet', href: '//cdn.bootcdn.net/ajax/libs/font-awesome/6.2.1/css/v4-shims.min.css'}]
]

export default head
